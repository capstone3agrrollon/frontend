import React, { useContext } from 'react';
// Import nextJS Link component for client-side navigation
import Link from 'next/link';
// Import necessary bootstrap components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

export default function NavBar() {
    // Consume the UserContext and destructure it to access the user state from the context provider
    const { user } = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg">
            <Link href="/">
                <a className="navbar-brand">AG Budget Tracker</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    {(user.id !== null)

                    ?<>
                        <Link href="/categories/displaycategories">
                            <a className="nav-link" role="button">Category</a>
                        </Link>
                        <Link href="/records">
                            <a className="nav-link" role="button">Records</a>
                        </Link>
                        <Link href="/monthlyexpense">
                            <a className="nav-link" role="button">Monthly Expense</a>
                        </Link>
                        <Link href="/monthlyincome">
                            <a className="nav-link" role="button">Monthly Income</a>
                        </Link>
                        <Link href="/trend">
                            <a className="nav-link" role="button">Trend</a>
                        </Link>
                        <Link href="/categorybreakdown">
                            <a className="nav-link" role="button">Breakdown</a>
                        </Link>
                        <Link href="/logout">
                            <a className="nav-link" role="button">Logout</a>
                        </Link>
                        
                    </>
                    :
                    <>
                        <Link href="/login">
                            <a className="nav-link" role="button">Login</a>
                        </Link>
                        <Link href="/register">
                            <a className="nav-link" role="button">Register</a>
                        </Link>
                    </>
                   
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}