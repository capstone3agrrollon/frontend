import PropTypes from 'prop-types';
// Import nextJS Link component for client-side navigation
import Link from 'next/link';
import Jumbotron from 'react-bootstrap/Jumbotron';
// Bootstrap grid system components
import { Row, Col } from 'react-bootstrap';

export default function Banner({data}) {

    // Destructure the data prop into separate variables
    const {title, content, destination, label} = data;

    return (
        <Row>
            <Col>
                <Jumbotron>
                    <h1>{title}</h1>
                    <p>{content}</p>
                    <Link href={destination}><a>{label}</a></Link>
                </Jumbotron>
            </Col>
        </Row>
    )

}

// Checks if the Banner component is getting the correct prop types
Banner.propTypes = {
    // shape() is used to check that a prop object conforms to a specific "shape" or data structure
    data: PropTypes.shape({
        // Define the properties and their expected types
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        destination: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
    })
}