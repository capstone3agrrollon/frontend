import React, { useContext, useEffect, useState } from "react";
import { Col } from "react-bootstrap";
import { Row } from "react-bootstrap";
import View from "../../components/View";
import UserContext from "../../UserContext";
import { Bar } from "react-chartjs-2";
import moment from "moment";

export default function index() {
  return (
    <View title={"Monthly Expense"}>
      <Row className="justify-content-center">
        <Col xs md="6">
          <h3>Monthly Expense</h3>
        </Col>
        <MonthlyIncome />
      </Row>
    </View>
  );
}

const MonthlyIncome = () => {
   const {user} = useContext(UserContext);
   const [month, setMonth] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"])
   const [balance, setBalance] = useState([])

   useEffect(() => {
       setBalance(month.map(month => {
           let total = 0;
           if(user.id){
               user.records.forEach(record => {
                   if(record.categoryType === "Expenses"){
                        if(moment(record.createdOn).format('MMMM') === month){
                            console.log(record.createdOn)
                            return(
                                total += record.amount
                            )
                        }
                   }
                   
               })
           }
           return total
       }))

   },[user.records])
   console.log(balance)

   const data ={
       labels: month,
       datasets: [{
            data: balance,
            label: 'Monthly Expense',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)'
       }]
   }
    return (
      <>
        <Bar data={data}/>
      </>
    );
  };