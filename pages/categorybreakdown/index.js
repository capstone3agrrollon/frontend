import React, { useContext, useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { Row } from "react-bootstrap";
import View from "../../components/View";
import UserContext from "../../UserContext";
import { colorRandomizer } from "../../helpers";
import { Pie } from "react-chartjs-2";
//import moment from "moment";

export default function index() {
  return (
    <View title={"Category Breakdown"}>
      <Row className="justify-content-center">
        <Col xs md="6">
          <h3>Category Breakdown</h3>
          <CategoryBreakdown />
        </Col>
      </Row>
    </View>
  );
}

const CategoryBreakdown = () => {
  const { user } = useContext(UserContext);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [amount, setAmount] = useState('');
  const [name, setName] = useState([]);
  const [bgColors, setBgColors] = useState([]);
  const [dataSets, setDataSets] = useState('');


  useEffect(() => {
    if (startDate !== "" && endDate !== ""){
        setDataSets(user.records.filter(record => {
            return (
                record.createdOn >= startDate && record.createdOn <= endDate
            )
        }))
    }
  }, [startDate, endDate])

  console.log(dataSets)
  

  useEffect(() => {
      if(dataSets){
          setAmount(dataSets.map(record => record.amount))
          setName(dataSets.map(record => record.categoryName))
          setBgColors(dataSets.map(()=>`#${colorRandomizer()}`))
      }
  }, [dataSets])

  const data = {
    labels: name,
    datasets: [
      {
        data: amount,
        backgroundColor: bgColors,
        hoverBackgroundColor: bgColors,
      },
    ],
  };

  return (
    <>
      <Form>
        <Form.Row>
          <Col>
            <h6>From</h6>
            <Form.Control
              type="date"
              value={startDate}
              onChange={e => setStartDate(e.target.value)}
            />
          </Col>
          <Col>
            <h6>To</h6>
            <Form.Control
              type="date"
              value={endDate}
              onChange={e => setEndDate(e.target.value)}
            />
          </Col>
        </Form.Row>
        
        <Pie data={data}/>
      </Form>

      
    </>
  );
};
