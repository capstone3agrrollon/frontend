import React, { useContext, useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { Row } from "react-bootstrap";
import View from "../../components/View";
import UserContext from "../../UserContext";
import { Line } from "react-chartjs-2";
import moment from "moment";


export default function index() {
  return (
    <View title={"Balance Trend"}>
      <Row className="justify-content-center">
        <Col xs md="12">
          <h3>Balance Trend</h3>
          <BalanceTrend />
        </Col>
      </Row>
    </View>
  );
}

const BalanceTrend = () => {
    const { user } = useContext(UserContext);
    const [records, setRecords] = useState([]);
    const [startDate, setStartDate] = useState ("");
    const [endDate, setEndDate] = useState ("");
    const [trendDates, setTrendDates] = useState('')
    const [totalBalance, setTotalBalance] = useState ([]);

    useEffect(() => {
        console.log(user.id);
        fetch(`${AppHelper.API_URL}/users/details`, {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
          .then(res => res.json())
          .then(data => {
                setRecords(data.records)
          });
      }, []);

    useEffect(() => {
        let datesArr = [];
        let amount = 0;
        let balance = [];

        records.forEach(record => {
            if((moment(record.createdOn).format("YYYY-MM-DD")>=startDate) && (moment(record.createdOn).format("YYYY-MM-DD")<=endDate)){
                if(datesArr.includes(moment(record.createdOn).format("YYYY-MM-DD")) !== true){
                    console.log(moment(record.createdOn).format("YYYY-MM-DD"))
                    if(record.categoryType === "Income"){
                        amount = amount + record.amount
                        balance.push(amount)
                    }
                    if(record.categoryType === "Expenses"){
                        amount = amount - record.amount
                        balance.push(amount)
                    }
                    datesArr.push(moment(record.createdOn).format("YYYY-MM-DD"))
                    console.log(datesArr)
                }else{
                    if(record.categoryType === "Income"){
                        amount = balance[balance.length - 1] + record.amount
                        balance.push(amount)
                    }
                    if(record.categoryType === "Expenses"){
                        amount = balance[balance.length - 1] - record.amount
                        balance.push(amount)
                    }
                }
            }
            setTrendDates(datesArr)
            setTotalBalance(balance)
        })
        
},[startDate, endDate])

    console.log(totalBalance)

    const data = {
        labels: trendDates,
        datasets: [{
            label:'Monthly Income',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            data: totalBalance
        }]
    }
  return (
    <>
      <Form>
        <Form.Row>
          <Col>
            <h6>From</h6>
            <Form.Control
              type="date"
              value={startDate}
              onChange={e => setStartDate(e.target.value)}
            />
          </Col>
          <Col>
            <h6>To</h6>
            <Form.Control
              type="date"
              value={endDate}
              onChange={e => setEndDate(e.target.value)}
            />
          </Col>
        </Form.Row>
        <Line data = {data}/>
      </Form>
      
    </>
  );
};
